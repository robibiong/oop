<?php
    require_once 'animal.php';

    class Ape extends Animal {
        //Property
        public $legs = 2;
        
        //Method 
        public function yell (){
            echo "Auouoooo";
        }
    }
?>